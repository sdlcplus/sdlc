package test.com.sdlc.jms.listener;

import static org.junit.Assert.*;

import java.rmi.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sdlc.jms.adapter.ConsumerAdapter;

public class ConsumerAdapterTest {

	private String json = "{vendorName:\"Microsofttest\",firstName:\"Anand\",lastName:\"Elangovan\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}"; 

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongo() {
		ConsumerAdapter adapter = new ConsumerAdapter();
		try {
			adapter.sendToMongo(json);
			assertNotNull(json);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

}
