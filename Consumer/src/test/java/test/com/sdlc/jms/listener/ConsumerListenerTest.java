/**
 * 
 */
package test.com.sdlc.jms.listener;

import static org.junit.Assert.*;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sdlc.jms.listener.ConsumerListener;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

/**
 * @author Anand
 *
 */
public class ConsumerListenerTest {
	
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttest1\",firstName:\"BobTest1\",lastName:\"SmithTest1\",address:\"123 Main test1\",city:\"TulsaTest1\",state:\"OKTest1\",zip:\"71345Test1\",email:\"Bob@microsoft.test1\",phoneNumber:\"test-123-test1\"}";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener)context.getBean("consumerListener");
		message = org.easymock.EasyMock.createMock(TextMessage.class);
	}
	
	@After
	public void tearDown() {
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testonMessage() throws JMSException {
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	}
}
