package com.sdlc.jms.adapter;

import java.rmi.UnknownHostException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;


@Component
public class ConsumerAdapter {
	
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	private MongoClient client;

	public void sendToMongo(String json) throws UnknownHostException {
		
		logger.info("sending to MongoDB");
		client = new MongoClient();
		MongoDatabase db = client.getDatabase("vendor");
		MongoCollection<Document> collection = db.getCollection("contact");
		logger.info("Converting JSON to DBObject");
		//DBObject obj = (DBObject)JSON.parse(json);
		Document doc = Document.parse(json);
		collection.insertOne(doc);
		logger.info("Sent to MongDB");
	}

	
}
