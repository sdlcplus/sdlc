package com.sdlc.jms.listener;

import java.rmi.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.sdlc.jms.adapter.ConsumerAdapter;


@Component
public class ConsumerListener implements MessageListener {
	
	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());

	@Autowired
	ConsumerAdapter adapter;
	@Autowired
	JmsTemplate jmsTemplate;

	@Override
	public void onMessage(Message message) {
		logger.info("In onMessage()");
		String json = null;
		
		if (message instanceof TextMessage) {
			try {
				json = ((TextMessage) message).getText();
				logger.info("sending to JSON to DB: " + json);
				adapter.sendToMongo(json);
			} catch (JMSException e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(message);
			} catch (UnknownHostException e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(message);
			}catch (Exception e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(message);
			}
		}
	}
}
