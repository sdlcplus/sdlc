package com.jms.mq.producer.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jms.mq.producer.model.Vendor;
import com.jms.mq.producer.service.MessagingService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ProducerController {

	private static final Logger logger = LogManager.getLogger(ProducerController.class.getName());
	@Autowired
	private MessagingService service;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String renderVendorPage(Vendor vendor, Model model) {
		logger.info("Rendering Index JSP");
		return "index";
	}

	@RequestMapping(value = "/vendor", method = RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("vendor") Vendor vendor, Model model) 
	{
		logger.info("processing vendor Object");
		logger.info(vendor.toString());
		service.process(vendor);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		mv.addObject("message", "Vendor Added Successfully!");
		vendor = new Vendor();
		mv.addObject("vendor",vendor);
		return mv;
	}

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	//@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

}
