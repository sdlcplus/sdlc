package com.jms.mq.producer.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.jms.mq.producer.model.Vendor;
import com.jms.mq.producer.sender.MessageSender;

@Component
public class MessagingService {

	private static final Logger logger = LogManager.getLogger(MessagingService.class.getName());
	@Autowired
	private MessageSender msgSender;
	public void process(Vendor vendor) {
		Gson gson = new Gson();
		String json = gson.toJson(vendor);
		logger.info("message :" + json);
		msgSender.send(json);
	}
}
