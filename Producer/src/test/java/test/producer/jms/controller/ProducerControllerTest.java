package test.producer.jms.controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.jms.mq.producer.controller.ProducerController;
import com.jms.mq.producer.model.Vendor;

public class ProducerControllerTest {
	
	private Vendor vendor;
	private Model model;
	private ProducerController controller;
	private ApplicationContext context;
	
	@Before
	public void setUp() throws Exception{
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		controller = (ProducerController) context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("UPS");
		vendor.setFirstName("Mithulan & Yugan & Poongs");
		vendor.setLastName("Anand");
		vendor.setAddress("Opaline");
		vendor.setCity("Chennai");
		vendor.setEmail("familt@gmail.com");
		vendor.setPhoneNumber("987654321");
		vendor.setState("TN");
		vendor.setZipCode("60032");
	}
	
	@After
	public void tearDown() throws Exception{
		
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", controller.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = controller.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
